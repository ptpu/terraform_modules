locals {
  acr_ip_rules = var.allowed_ip
}

resource "azurerm_container_registry" "acr" {
  name                = var.resource_name
  resource_group_name = var.rg_name
  location            = var.location
  sku                 = "Premium"
  admin_enabled       = true

  network_rule_set {
    default_action = "Deny"

    ip_rule = [
      for ip in local.acr_ip_rules : {
        action   = "Allow"
        ip_range = ip
      }
    ]
  }

  tags = {
    Terraform   = "true"
    Environment = var.environment
  }
}