variable "location" {
  type        = string
  description = "LocationId of an Azure Datacenter"
}

variable "environment" {
  type        = string
  description = "Short-Name of environment which will be updated."
}

variable "rg_name" {
  type        = string
  description = "Name of the resource group"
}

variable "resource_name" {
  type        = string
  description = "Name of the resource which will be created in this module."
}

# Private Link Endpoint
variable "pep_subnet_id" {
  type        = string
  description = "Id of the Subnet where Private-Endpoint of Container Registry needs to be created."
}

variable "pep_dns_zone_name" {
  type        = string
  description = "The name of DNS Zone to which the Private Endpoint needs to be connected."
}

variable "rg_dnszone_name" {
  type        = string
  description = "Name of the resource group in which DNS Zone is placed"
}

# Firewall
variable "allowed_ip" {
  type        = list(string)
  description = "List of IP addresses in CIDR format."
  default     = []
}