resource "azurerm_private_endpoint" "pep" {
  name                = format("%s-pep", var.container_registry_name)
  location            = var.location
  resource_group_name = var.rg_name
  subnet_id           = var.pep_subnet_id

  private_service_connection {
    name                           = format("%s-pep-connection", var.container_registry_name)
    private_connection_resource_id = var.container_registry_id
    subresource_names              = ["registry"]
    is_manual_connection           = false
  }
}

data "azurerm_network_interface" "nic" {
  name                = azurerm_private_endpoint.pep.network_interface[0].name
  resource_group_name = var.rg_name

  depends_on = [
    azurerm_private_endpoint.pep
  ]
}

resource "azurerm_private_dns_a_record" "pep_dns_record_data" {
  name                = lower(format("%s.%s.data", var.container_registry_name, var.location))
  zone_name           = var.pep_dns_zone_name
  resource_group_name = var.rg_dnszone_name
  ttl                 = 3600
  records             = [data.azurerm_network_interface.nic.private_ip_addresses[0]]

  depends_on = [
    azurerm_private_endpoint.pep
  ]
}

resource "azurerm_private_dns_a_record" "pep_dns_record" {
  name                = lower(var.container_registry_name)
  zone_name           = var.pep_dns_zone_name
  resource_group_name = var.rg_dnszone_name
  ttl                 = 3600
  records             = [data.azurerm_network_interface.nic.private_ip_addresses[1]]

  depends_on = [
    azurerm_private_endpoint.pep
  ]
}