variable "location" {
  type        = string
  description = "LocationId of an Azure Datacenter"
}

variable "environment" {
  type        = string
  description = "Short-Name of evnironment which will be updated"
}

variable "rg_name" {
  type        = string
  description = "Name of the resource group"
}

variable "container_registry_name" {
  type        = string
  description = "Name of the container registry"
}

variable "container_registry_id" {
  type        = string
  description = "Resource id of the container registry"
}

# Private Link Endpoint
variable "pep_subnet_id" {
  type        = string
  description = "Id of the Subnet where Private-Endpoint of Container Registry needs to be created."
}

variable "pep_dns_zone_name" {
  type        = string
  description = "The name of DNS Zone to which the Private Endpoint needs to be connected."
}

variable "rg_dnszone_name" {
  type        = string
  description = "Name of the resource group in which DNS Zone is placed"
}