variable "rg_name" {
  type        = string
  description = "Name of the resource group"
}

variable "dns_zone_name" {
  type        = string
  description = "Name of the dns zone"
}